﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour {

    public float _speed;

    public bool _move;

    public int _damage = 10;



    private void Start()
    {
        _move = true;
    }
    // Update is called once per frame
    void Update()
    {
        if(_move)
            transform.Translate(1f * -_speed * Time.deltaTime, 0f, 0f);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Wall")
        {
            Debug.Log("Entre");
            _move = false;
            StartCoroutine(MakeDamage());
        }
    }

    IEnumerator MakeDamage()
    {
        while (true)
        {
            Debug.Log("Hago daño a la pared");
            yield return new WaitForSeconds(2f);
        }
    }

    /*
     Movimiento del slime hasta la pared del castillo
     Tocar castillo = cambio de estado a golpe
     Muerte con toque de flecha
     
     */
}
