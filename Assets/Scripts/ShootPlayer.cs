﻿using UnityEngine;

public class ShootPlayer : MonoBehaviour
{
    public Transform _initialPosition; // Posicion inicial
    public GameObject _arrow; 

    //Delay Arco
    public float _actualTime = 1;
    public float _delayShoot = 1;

    void Update()
    {
        _actualTime += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && _actualTime >= _delayShoot) // Click Izquierdo mas tiempo de recarga
        {
            

            Plane _plane = new Plane(Camera.main.transform.forward, transform.position); // Plano imaginario para normalizar la posicion del mouse

            Ray _pointMouse = Camera.main.ScreenPointToRay(Input.mousePosition);

            float _out;

            if (_plane.Raycast(_pointMouse, out _out)) 
            {
                Vector3 _pointNormalized = _pointMouse.GetPoint(_out);
                GameObject _instantiatedArrow = Instantiate(_arrow, _initialPosition.position, _initialPosition.rotation) as GameObject;


                _instantiatedArrow.gameObject.GetComponent<ArrowScript>()._mousePoint = _pointNormalized;

                _actualTime = 0;

            }
            
            
        }
    }



}
