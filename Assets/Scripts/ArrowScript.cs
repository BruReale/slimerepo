﻿using UnityEngine;

public class ArrowScript : MonoBehaviour {

    public Vector3 _mousePoint;

    public float _speedArrow;

    void Update()
    {
        
        float step = _speedArrow * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _mousePoint, step);

        if (transform.position == _mousePoint)
            Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.tag == "Slime")
        {

            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

    /*
     Movimiento de flecha hasta el click del mouse
     Destruir al tocar suelo o slime
     Determinar velocidad segun distancia
     */
}
