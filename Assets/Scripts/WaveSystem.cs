﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaveSystem : MonoBehaviour {

    #region Waves Variables
    public GameObject[] _slime;

    public float _possibleSpawnPosition1;
    public float _possibleSpawnPosition2;

    public int _slimeInScene;

    public float _waitForAnotherSpawn;
    public float _waitStartSpawn;
    public float _waitWave;

    public int _slimeDefeated;
    #endregion


    #region Wall Variables
    public int _health;
    #endregion


    private static WaveSystem instance = null;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
    }



    // Use this for initialization
    void Start () {

        _health = 100;

        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 0:
                StartCoroutine(SpawnWaves1());
                break;
        }
        
	}

    private void Update()
    {

        if (_health <= 0)
        {
            StopAllCoroutines();

            // Restart todas las waves
        }
        /*
         Si la pared se queda sin vida se pierde el nivel
         Si el nivel se supera se detiene la rutina y se muestra la pantalla de ganaste
         */
    }

    IEnumerator SpawnWaves1()
    {
        
            yield return new WaitForSeconds(_waitStartSpawn);
            while (_slimeDefeated < 10)
            {
                for (int i = 0; i < _slimeInScene; i++)
                {
                    Vector2 _spawnPosition = new Vector2(9.6f, Random.Range(_possibleSpawnPosition1, _possibleSpawnPosition2));

                    Instantiate(_slime[0], _spawnPosition, Quaternion.identity);
                    yield return new WaitForSeconds(_waitForAnotherSpawn);
                }
                yield return new WaitForSeconds(_waitWave);
            }
          
    }
}
